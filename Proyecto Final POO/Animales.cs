﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_Final_POO
{
    /**
     * 
     * -------------------- POO_01T - G06
     * -------------------- INTEGRANTES
     * ** Román Antonio González Montano    | GM181937
     * ** Kevin Osvaldo Marroquín Medrano   | MM161915
     * ** Juan José Magaña Moreira          | MM210126
     * ** Ricardo Antonio Quijano Vasquez   | QV211321
     * ** Kevin Osmaro Sibrián López        | Sl210844
     * 
     * -------------------- URL SOLUCIÓN
     * ** https://gitlab.com/marroquin_oz/zoologico_app-poo01t_g06
     * 
     */
    abstract class Animales
    {
        //Definimos los prototipos
        private string nombreComun;
        private string nombreCientifico;
        private string clasificacion;
        private string descripcionHabitad;
        private string foto;

        // GETTERS Y SETTERS SOLO PARA CLASE ABSTRACTA DE MANERA protected
        protected string NombreComun { get => nombreComun; set => nombreComun = value; }
        protected string NombreCientifico { get => nombreCientifico; set => nombreCientifico = value; }
        protected string Foto { get => foto; set => foto = value; }
        protected string Clasificacion { get => clasificacion; set => clasificacion = value; }
        protected string DescripcionHabitad { get => descripcionHabitad; set => descripcionHabitad = value; }

        // METODOS ABSTRACTOS
        public abstract int validarVacios(
            string nombreComun,
            string nombreCientifico,
            string clasificacion,
            string habitad,
            string param1,
            string param2,
            string param3,
            string rutaFoto
        );
        public abstract bool setAnimal(
            string nombreComun,
            string nombreCientifico,
            string clasificacion,
            string habitad,
            string param1,
            string param2,
            string param3,
            string rutaFoto
        );
        public abstract Dictionary<string, Object> getAnimal();
        public abstract void Save();
    }
}

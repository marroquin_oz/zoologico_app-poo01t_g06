using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Proyecto_Final_POO
{
    /**
     * 
     * -------------------- POO_01T - G06
     * -------------------- INTEGRANTES
     * ** Román Antonio González Montano    | GM181937
     * ** Kevin Osvaldo Marroquín Medrano   | MM161915
     * ** Juan José Magaña Moreira          | MM210126
     * ** Ricardo Antonio Quijano Vasquez   | QV211321
     * ** Kevin Osmaro Sibrián López        | Sl210844
     * 
     * -------------------- URL SOLUCIÓN
     * ** https://gitlab.com/marroquin_oz/zoologico_app-poo01t_g06
     * 
     */
    class Peces : Animales
    {
        private string tipoEsqueleto;
        private string tipoPez;
        private string tipoAcuristica;
        private XmlDocument database = new XmlDocument();

        // GETTERS Y SETTERS SIN USAR, YA QUE TENEMOS METODOS ESPECIFICOS PARA OBTENER Y ASIGNAR LAS PROPIEDADES

        public override int validarVacios(string nombreComun, string nombreCientifico, string clasificacion, string habitad, string tipoEsqueleto, string tipoPez, string tipoAcuristica, string rutaFoto) 
        {
            if (nombreComun == "" || nombreCientifico == "" || clasificacion == "Seleccionar" || habitad == "Seleccionar" || tipoEsqueleto == "Seleccionar" || tipoPez == "Seleccionar" || tipoAcuristica == "Seleccionar")
            {
                MessageBox.Show("Favor completar todos los campos");
                return 0;
            }
            else 
            {
                //Validamos que se haya seleccionado una imagen
                if (rutaFoto == "")
                {
                    MessageBox.Show("Seleccione una imagen para el animal a registrar");
                    return 0;
                }
                
                return 1;
            }
        }

        public override bool setAnimal(
            string nombreComun,
            string nombreCientifico,
            string clasificacion,
            string habitad,
            string param1,
            string param2,
            string param3,
            string rutaFoto
        )
        {
            this.NombreComun = nombreComun;
            this.NombreCientifico = nombreCientifico;
            this.Clasificacion = clasificacion;
            this.DescripcionHabitad = habitad;
            this.tipoEsqueleto = param1;
            this.tipoPez = param2;
            this.tipoAcuristica = param3;
            this.Foto = rutaFoto;

            return true;
        }

        public override Dictionary<string, Object> getAnimal()
        {
            return new Dictionary<string, Object>() {
                {"nombreComun", this.NombreComun},
                {"nombreCientifico", this.NombreCientifico},
                {"clasificacion", this.Clasificacion},
                {"habitad", this.DescripcionHabitad},
                {"tipoEsqueleto", this.tipoEsqueleto},
                {"tipoPez", this.tipoPez},
                {"tipoAcuristica", this.tipoAcuristica},
                {"rutaFoto", this.Foto}
            };
        }

        public override void Save()
        {
            database.Load("database.xml");
            XmlNode parent = database.CreateNode(XmlNodeType.Element, "Animal", "");
            /**************************/
            parent.AppendChild(database.CreateElement("nombreComun"));
            parent.LastChild.InnerText = this.NombreComun;
            parent.AppendChild(database.CreateElement("nombreCientifico"));
            parent.LastChild.InnerText = this.NombreCientifico;
            parent.AppendChild(database.CreateElement("clasificacion"));
            parent.LastChild.InnerText = this.Clasificacion;
            parent.AppendChild(database.CreateElement("habitad"));
            parent.LastChild.InnerText = this.DescripcionHabitad;
            parent.AppendChild(database.CreateElement("tipoEsqueleto"));
            parent.LastChild.InnerText = this.tipoEsqueleto;
            parent.AppendChild(database.CreateElement("tipoPez"));
            parent.LastChild.InnerText = this.tipoPez;
            parent.AppendChild(database.CreateElement("tipoAcuristica"));
            parent.LastChild.InnerText = this.tipoAcuristica;
            parent.AppendChild(database.CreateElement("rutaFoto"));
            parent.LastChild.InnerText = this.Foto;
            /*************************/
            database.SelectSingleNode(@"Database/Peces[1]").AppendChild(parent);
            database.Save("database.xml");
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Proyecto_Final_POO
{
    /**
     * 
     * -------------------- POO_01T - G06
     * -------------------- INTEGRANTES
     * ** Román Antonio González Montano    | GM181937
     * ** Kevin Osvaldo Marroquín Medrano   | MM161915
     * ** Juan José Magaña Moreira          | MM210126
     * ** Ricardo Antonio Quijano Vasquez   | QV211321
     * ** Kevin Osmaro Sibrián López        | Sl210844
     * 
     * -------------------- URL SOLUCIÓN
     * ** https://gitlab.com/marroquin_oz/zoologico_app-poo01t_g06
     * 
     */
    class Mamiferos : Animales
    {
        //Definimos los prototipos de la clase mamíferos
        private string genero;
        private string tamano;
        private string tipoApoyo;
        private XmlDocument database = new XmlDocument();

        // GETTERS Y SETTERS SIN USAR, YA QUE TENEMOS METODOS ESPECIFICOS PARA OBTENER Y ASIGNAR LAS PROPIEDADES

        public override int validarVacios(string nombreComun, string nombreCientifico, string clasificacion, string habitad, string genero, string tamano, string tipoApoyo, string rutaFoto)
        {
            if (nombreComun == "" || nombreCientifico == "" || clasificacion == "Seleccionar" || habitad == "Seleccionar" || genero == "Seleccionar" || tamano == "" || tipoApoyo == "")
            {
                MessageBox.Show("Por favor, completar todos los campos");
                return 0;
            }
            else
            {
                //Validamos tamano solo numeros
                if (tamano != "")
                {
                    double tamanoInt;
                    if (!double.TryParse(tamano, out tamanoInt))
                    {
                        MessageBox.Show("Por favor, ingresar un número en el campo tamaño");
                        return 0;
                    }
                }

                //Validamos que se haya seleccionado una imagen
                if (rutaFoto == "")
                {
                    MessageBox.Show("Seleccione una imagen para el animal a registrar");
                    return 0;
                }

                return 1;
            }
        }

        public override bool setAnimal(
            string nombreComun,
            string nombreCientifico,
            string clasificacion,
            string habitad,
            string param1,
            string param2,
            string param3,
            string rutaFoto
        ) {
            this.NombreComun = nombreComun;
            this.NombreCientifico = nombreCientifico;
            this.Clasificacion = clasificacion;
            this.DescripcionHabitad = habitad;
            this.genero = param1;
            this.tamano = param2;
            this.tipoApoyo = param3;
            this.Foto = rutaFoto;

            return true;
        }

        public override Dictionary<string, Object> getAnimal()
        {
            return new Dictionary<string, Object>() {
                {"nombreComun", this.NombreComun},
                {"nombreCientifico", this.NombreCientifico},
                {"clasificacion", this.Clasificacion},
                {"habitad", this.DescripcionHabitad},
                {"genero", this.genero},
                {"tamano", this.tamano},
                {"tipoApoyo", this.tipoApoyo},
                {"rutaFoto", this.Foto}
            };
        }

        public override void Save()
        {
            database.Load("database.xml");
            XmlNode parent = database.CreateNode(XmlNodeType.Element, "Animal", "");
            /**************************/
            parent.AppendChild(database.CreateElement("nombreComun"));
            parent.LastChild.InnerText = this.NombreComun;
            parent.AppendChild(database.CreateElement("nombreCientifico"));
            parent.LastChild.InnerText = this.NombreCientifico;
            parent.AppendChild(database.CreateElement("clasificacion"));
            parent.LastChild.InnerText = this.Clasificacion;
            parent.AppendChild(database.CreateElement("habitad"));
            parent.LastChild.InnerText = this.DescripcionHabitad;
            parent.AppendChild(database.CreateElement("genero"));
            parent.LastChild.InnerText = this.genero;
            parent.AppendChild(database.CreateElement("tamano"));
            parent.LastChild.InnerText = this.tamano;
            parent.AppendChild(database.CreateElement("tipoApoyo"));
            parent.LastChild.InnerText = this.tipoApoyo;
            parent.AppendChild(database.CreateElement("rutaFoto"));
            parent.LastChild.InnerText = this.Foto;
            /*************************/
            database.SelectSingleNode(@"Database/Mamiferos[1]").AppendChild(parent);
            database.Save("database.xml");
        }

    }
}

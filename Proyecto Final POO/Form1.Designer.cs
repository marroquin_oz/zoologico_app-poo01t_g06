﻿
namespace Proyecto_Final_POO
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.TabReptiles = new System.Windows.Forms.TabPage();
            this.cbClasReptil2 = new System.Windows.Forms.ComboBox();
            this.cbTipoEscama = new System.Windows.Forms.ComboBox();
            this.cbClasReptil1 = new System.Windows.Forms.ComboBox();
            this.txbRutaRe = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.btnExplorarRe = new System.Windows.Forms.Button();
            this.dgvReptiles = new System.Windows.Forms.DataGridView();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btnAgregarRe = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.txbColor = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txbDesHabitadRe = new System.Windows.Forms.TextBox();
            this.txbNomCienRe = new System.Windows.Forms.TextBox();
            this.txbNomComRe = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pbReptil = new System.Windows.Forms.PictureBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.cbHabitatMamifero = new System.Windows.Forms.ComboBox();
            this.cbGeneroMamifero = new System.Windows.Forms.ComboBox();
            this.cbClasMamifero = new System.Windows.Forms.ComboBox();
            this.txbTamaño = new System.Windows.Forms.TextBox();
            this.txbRutaMa = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.btnExplorarMa = new System.Windows.Forms.Button();
            this.dgvMamiferos = new System.Windows.Forms.DataGridView();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.btnAgregarMa = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.txbTipoCaminar = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txbNomCienMa = new System.Windows.Forms.TextBox();
            this.txbNomComMa = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.pbMamifero = new System.Windows.Forms.PictureBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.cbAcuristica = new System.Windows.Forms.ComboBox();
            this.cbTipoPez = new System.Windows.Forms.ComboBox();
            this.cbtipoesqueletopec = new System.Windows.Forms.ComboBox();
            this.cbHabitatPes1 = new System.Windows.Forms.ComboBox();
            this.cbClasPes1 = new System.Windows.Forms.ComboBox();
            this.label36 = new System.Windows.Forms.Label();
            this.btnAgregarPe = new System.Windows.Forms.Button();
            this.label34 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.txtNombreCienPe = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.txtNombreComPe = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.dgvPeces = new System.Windows.Forms.DataGridView();
            this.btnExplorarPe = new System.Windows.Forms.Button();
            this.pbPeces = new System.Windows.Forms.PictureBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtRutaPe = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.cbReproduccionAntrop = new System.Windows.Forms.ComboBox();
            this.cbAlimentacionAntrop = new System.Windows.Forms.ComboBox();
            this.cbPuedeSer = new System.Windows.Forms.ComboBox();
            this.cbHabitatAntropodo = new System.Windows.Forms.ComboBox();
            this.cbClasAntropodo = new System.Windows.Forms.ComboBox();
            this.label44 = new System.Windows.Forms.Label();
            this.btnAgregarAntropodo = new System.Windows.Forms.Button();
            this.label43 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.txtNombreCientificoAntro = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.txtNomComunAntro = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.dgvAntropodos = new System.Windows.Forms.DataGridView();
            this.label35 = new System.Windows.Forms.Label();
            this.btnExplorarAntro = new System.Windows.Forms.Button();
            this.pbAntropodos = new System.Windows.Forms.PictureBox();
            this.txtRutaAntropodos = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.fileExplorer = new System.Windows.Forms.OpenFileDialog();
            this.tabControl1.SuspendLayout();
            this.TabReptiles.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReptiles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbReptil)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMamiferos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMamifero)).BeginInit();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPeces)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPeces)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAntropodos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAntropodos)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.TabReptiles);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(9, 4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(648, 582);
            this.tabControl1.TabIndex = 26;
            // 
            // TabReptiles
            // 
            this.TabReptiles.Controls.Add(this.cbClasReptil2);
            this.TabReptiles.Controls.Add(this.cbTipoEscama);
            this.TabReptiles.Controls.Add(this.cbClasReptil1);
            this.TabReptiles.Controls.Add(this.txbRutaRe);
            this.TabReptiles.Controls.Add(this.label11);
            this.TabReptiles.Controls.Add(this.btnExplorarRe);
            this.TabReptiles.Controls.Add(this.dgvReptiles);
            this.TabReptiles.Controls.Add(this.label10);
            this.TabReptiles.Controls.Add(this.label9);
            this.TabReptiles.Controls.Add(this.btnAgregarRe);
            this.TabReptiles.Controls.Add(this.label8);
            this.TabReptiles.Controls.Add(this.txbColor);
            this.TabReptiles.Controls.Add(this.label7);
            this.TabReptiles.Controls.Add(this.label6);
            this.TabReptiles.Controls.Add(this.label5);
            this.TabReptiles.Controls.Add(this.label4);
            this.TabReptiles.Controls.Add(this.label3);
            this.TabReptiles.Controls.Add(this.label2);
            this.TabReptiles.Controls.Add(this.txbDesHabitadRe);
            this.TabReptiles.Controls.Add(this.txbNomCienRe);
            this.TabReptiles.Controls.Add(this.txbNomComRe);
            this.TabReptiles.Controls.Add(this.label1);
            this.TabReptiles.Controls.Add(this.pbReptil);
            this.TabReptiles.Location = new System.Drawing.Point(4, 22);
            this.TabReptiles.Name = "TabReptiles";
            this.TabReptiles.Padding = new System.Windows.Forms.Padding(3);
            this.TabReptiles.Size = new System.Drawing.Size(640, 556);
            this.TabReptiles.TabIndex = 0;
            this.TabReptiles.Text = "Reptiles";
            this.TabReptiles.UseVisualStyleBackColor = true;
            // 
            // cbClasReptil2
            // 
            this.cbClasReptil2.FormattingEnabled = true;
            this.cbClasReptil2.Items.AddRange(new object[] {
            "Seleccionar",
            "Terrestre",
            "Acuatico"});
            this.cbClasReptil2.Location = new System.Drawing.Point(290, 103);
            this.cbClasReptil2.Name = "cbClasReptil2";
            this.cbClasReptil2.Size = new System.Drawing.Size(150, 21);
            this.cbClasReptil2.TabIndex = 49;
            // 
            // cbTipoEscama
            // 
            this.cbTipoEscama.FormattingEnabled = true;
            this.cbTipoEscama.Items.AddRange(new object[] {
            "Seleccionar",
            "Cicloides",
            "Granulares"});
            this.cbTipoEscama.Location = new System.Drawing.Point(476, 151);
            this.cbTipoEscama.Name = "cbTipoEscama";
            this.cbTipoEscama.Size = new System.Drawing.Size(141, 21);
            this.cbTipoEscama.TabIndex = 48;
            // 
            // cbClasReptil1
            // 
            this.cbClasReptil1.FormattingEnabled = true;
            this.cbClasReptil1.Items.AddRange(new object[] {
            "Seleccionar",
            "Tortuga",
            "Lagarto",
            "Serpiente",
            "Rincocefalo",
            "Cocodrilos"});
            this.cbClasReptil1.Location = new System.Drawing.Point(290, 151);
            this.cbClasReptil1.Name = "cbClasReptil1";
            this.cbClasReptil1.Size = new System.Drawing.Size(150, 21);
            this.cbClasReptil1.TabIndex = 47;
            // 
            // txbRutaRe
            // 
            this.txbRutaRe.Location = new System.Drawing.Point(22, 59);
            this.txbRutaRe.Name = "txbRutaRe";
            this.txbRutaRe.ReadOnly = true;
            this.txbRutaRe.Size = new System.Drawing.Size(229, 20);
            this.txbRutaRe.TabIndex = 10;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(18, 31);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(126, 19);
            this.label11.TabIndex = 46;
            this.label11.Text = "Foto del Animal";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnExplorarRe
            // 
            this.btnExplorarRe.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnExplorarRe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExplorarRe.ForeColor = System.Drawing.Color.White;
            this.btnExplorarRe.Location = new System.Drawing.Point(176, 30);
            this.btnExplorarRe.Name = "btnExplorarRe";
            this.btnExplorarRe.Size = new System.Drawing.Size(75, 23);
            this.btnExplorarRe.TabIndex = 9;
            this.btnExplorarRe.Text = "Explorar";
            this.btnExplorarRe.UseVisualStyleBackColor = false;
            this.btnExplorarRe.Click += new System.EventHandler(this.btnExplorarRe_Click);
            // 
            // dgvReptiles
            // 
            this.dgvReptiles.AllowUserToAddRows = false;
            this.dgvReptiles.AllowUserToDeleteRows = false;
            this.dgvReptiles.AllowUserToOrderColumns = true;
            this.dgvReptiles.Location = new System.Drawing.Point(8, 278);
            this.dgvReptiles.Name = "dgvReptiles";
            this.dgvReptiles.ReadOnly = true;
            this.dgvReptiles.RowHeadersVisible = false;
            this.dgvReptiles.RowTemplate.Height = 100;
            this.dgvReptiles.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvReptiles.Size = new System.Drawing.Size(623, 267);
            this.dgvReptiles.TabIndex = 8;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.label10.Location = new System.Drawing.Point(229, 253);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(164, 22);
            this.label10.TabIndex = 43;
            this.label10.Text = "LISTA REPTILES";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 238);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(622, 13);
            this.label9.TabIndex = 42;
            this.label9.Text = resources.GetString("label9.Text");
            // 
            // btnAgregarRe
            // 
            this.btnAgregarRe.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnAgregarRe.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnAgregarRe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgregarRe.ForeColor = System.Drawing.Color.White;
            this.btnAgregarRe.Location = new System.Drawing.Point(476, 200);
            this.btnAgregarRe.Name = "btnAgregarRe";
            this.btnAgregarRe.Size = new System.Drawing.Size(141, 23);
            this.btnAgregarRe.TabIndex = 7;
            this.btnAgregarRe.Text = "Agregar";
            this.btnAgregarRe.UseVisualStyleBackColor = false;
            this.btnAgregarRe.Click += new System.EventHandler(this.btnAgregarRe_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(286, 174);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(51, 23);
            this.label8.TabIndex = 40;
            this.label8.Text = "Color";
            // 
            // txbColor
            // 
            this.txbColor.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbColor.Location = new System.Drawing.Point(290, 200);
            this.txbColor.Name = "txbColor";
            this.txbColor.Size = new System.Drawing.Size(150, 20);
            this.txbColor.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(472, 125);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(131, 23);
            this.label7.TabIndex = 38;
            this.label7.Text = "Tipo de Escama";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(286, 125);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 23);
            this.label6.TabIndex = 37;
            this.label6.Text = "Tipo Reptil";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(472, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(158, 23);
            this.label5.TabIndex = 36;
            this.label5.Text = "Descripción Habitat";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(286, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 23);
            this.label4.TabIndex = 35;
            this.label4.Text = "Clasificación";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(472, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(145, 23);
            this.label3.TabIndex = 34;
            this.label3.Text = "Nombre Cientifico";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(286, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 23);
            this.label2.TabIndex = 33;
            this.label2.Text = "Nombre Común";
            // 
            // txbDesHabitadRe
            // 
            this.txbDesHabitadRe.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbDesHabitadRe.Location = new System.Drawing.Point(476, 102);
            this.txbDesHabitadRe.Name = "txbDesHabitadRe";
            this.txbDesHabitadRe.Size = new System.Drawing.Size(141, 20);
            this.txbDesHabitadRe.TabIndex = 3;
            // 
            // txbNomCienRe
            // 
            this.txbNomCienRe.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbNomCienRe.Location = new System.Drawing.Point(476, 53);
            this.txbNomCienRe.Name = "txbNomCienRe";
            this.txbNomCienRe.Size = new System.Drawing.Size(141, 20);
            this.txbNomCienRe.TabIndex = 1;
            // 
            // txbNomComRe
            // 
            this.txbNomComRe.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbNomComRe.Location = new System.Drawing.Point(290, 53);
            this.txbNomComRe.Name = "txbNomComRe";
            this.txbNomComRe.Size = new System.Drawing.Size(150, 20);
            this.txbNomComRe.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.label1.Location = new System.Drawing.Point(49, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(159, 24);
            this.label1.TabIndex = 27;
            this.label1.Text = "NUEVO REPTIL";
            // 
            // pbReptil
            // 
            this.pbReptil.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pbReptil.Location = new System.Drawing.Point(22, 85);
            this.pbReptil.Name = "pbReptil";
            this.pbReptil.Size = new System.Drawing.Size(229, 150);
            this.pbReptil.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbReptil.TabIndex = 26;
            this.pbReptil.TabStop = false;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.cbHabitatMamifero);
            this.tabPage2.Controls.Add(this.cbGeneroMamifero);
            this.tabPage2.Controls.Add(this.cbClasMamifero);
            this.tabPage2.Controls.Add(this.txbTamaño);
            this.tabPage2.Controls.Add(this.txbRutaMa);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.btnExplorarMa);
            this.tabPage2.Controls.Add(this.dgvMamiferos);
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Controls.Add(this.label14);
            this.tabPage2.Controls.Add(this.btnAgregarMa);
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Controls.Add(this.txbTipoCaminar);
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Controls.Add(this.label18);
            this.tabPage2.Controls.Add(this.label19);
            this.tabPage2.Controls.Add(this.label20);
            this.tabPage2.Controls.Add(this.label21);
            this.tabPage2.Controls.Add(this.txbNomCienMa);
            this.tabPage2.Controls.Add(this.txbNomComMa);
            this.tabPage2.Controls.Add(this.label22);
            this.tabPage2.Controls.Add(this.pbMamifero);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(640, 556);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Mamiferos";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // cbHabitatMamifero
            // 
            this.cbHabitatMamifero.FormattingEnabled = true;
            this.cbHabitatMamifero.Items.AddRange(new object[] {
            "Seleccionar",
            "Zona Polar",
            "Zona Acuatica",
            "Bioma Taiga",
            "Zona Estepa",
            "Zona Tropical y Subtropicales",
            "Zona Desertica",
            "Selva Tropical",
            "Zona Mediterranea"});
            this.cbHabitatMamifero.Location = new System.Drawing.Point(476, 101);
            this.cbHabitatMamifero.Name = "cbHabitatMamifero";
            this.cbHabitatMamifero.Size = new System.Drawing.Size(141, 21);
            this.cbHabitatMamifero.TabIndex = 72;
            // 
            // cbGeneroMamifero
            // 
            this.cbGeneroMamifero.FormattingEnabled = true;
            this.cbGeneroMamifero.Items.AddRange(new object[] {
            "Seleccionar",
            "Macho",
            "Hembra"});
            this.cbGeneroMamifero.Location = new System.Drawing.Point(290, 150);
            this.cbGeneroMamifero.Name = "cbGeneroMamifero";
            this.cbGeneroMamifero.Size = new System.Drawing.Size(150, 21);
            this.cbGeneroMamifero.TabIndex = 71;
            // 
            // cbClasMamifero
            // 
            this.cbClasMamifero.FormattingEnabled = true;
            this.cbClasMamifero.Items.AddRange(new object[] {
            "Seleccionar",
            "Placentarios",
            "Eutarios",
            "Marsupiales",
            "Metaterios",
            "Monotremas",
            "Prototerios"});
            this.cbClasMamifero.Location = new System.Drawing.Point(290, 103);
            this.cbClasMamifero.Name = "cbClasMamifero";
            this.cbClasMamifero.Size = new System.Drawing.Size(150, 21);
            this.cbClasMamifero.TabIndex = 70;
            // 
            // txbTamaño
            // 
            this.txbTamaño.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbTamaño.Location = new System.Drawing.Point(476, 151);
            this.txbTamaño.Name = "txbTamaño";
            this.txbTamaño.Size = new System.Drawing.Size(141, 20);
            this.txbTamaño.TabIndex = 7;
            // 
            // txbRutaMa
            // 
            this.txbRutaMa.Location = new System.Drawing.Point(22, 59);
            this.txbRutaMa.Name = "txbRutaMa";
            this.txbRutaMa.ReadOnly = true;
            this.txbRutaMa.Size = new System.Drawing.Size(229, 20);
            this.txbRutaMa.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(18, 31);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(126, 19);
            this.label12.TabIndex = 69;
            this.label12.Text = "Foto del Animal";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnExplorarMa
            // 
            this.btnExplorarMa.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnExplorarMa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExplorarMa.ForeColor = System.Drawing.Color.White;
            this.btnExplorarMa.Location = new System.Drawing.Point(176, 30);
            this.btnExplorarMa.Name = "btnExplorarMa";
            this.btnExplorarMa.Size = new System.Drawing.Size(75, 23);
            this.btnExplorarMa.TabIndex = 0;
            this.btnExplorarMa.Text = "Explorar";
            this.btnExplorarMa.UseVisualStyleBackColor = false;
            this.btnExplorarMa.Click += new System.EventHandler(this.btnExplorarMa_Click);
            // 
            // dgvMamiferos
            // 
            this.dgvMamiferos.AllowUserToAddRows = false;
            this.dgvMamiferos.AllowUserToDeleteRows = false;
            this.dgvMamiferos.Location = new System.Drawing.Point(8, 278);
            this.dgvMamiferos.Name = "dgvMamiferos";
            this.dgvMamiferos.ReadOnly = true;
            this.dgvMamiferos.RowHeadersVisible = false;
            this.dgvMamiferos.RowTemplate.Height = 100;
            this.dgvMamiferos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMamiferos.Size = new System.Drawing.Size(623, 267);
            this.dgvMamiferos.TabIndex = 10;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.label13.Location = new System.Drawing.Point(228, 253);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(187, 22);
            this.label13.TabIndex = 66;
            this.label13.Text = "LISTA MAMIFEROS";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(8, 238);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(622, 13);
            this.label14.TabIndex = 65;
            this.label14.Text = resources.GetString("label14.Text");
            // 
            // btnAgregarMa
            // 
            this.btnAgregarMa.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnAgregarMa.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnAgregarMa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgregarMa.ForeColor = System.Drawing.Color.White;
            this.btnAgregarMa.Location = new System.Drawing.Point(476, 200);
            this.btnAgregarMa.Name = "btnAgregarMa";
            this.btnAgregarMa.Size = new System.Drawing.Size(141, 23);
            this.btnAgregarMa.TabIndex = 9;
            this.btnAgregarMa.Text = "Agregar";
            this.btnAgregarMa.UseVisualStyleBackColor = false;
            this.btnAgregarMa.Click += new System.EventHandler(this.btnAgregarMa_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(286, 174);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(178, 23);
            this.label15.TabIndex = 63;
            this.label15.Text = "Tipo apoyo al caminar";
            // 
            // txbTipoCaminar
            // 
            this.txbTipoCaminar.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbTipoCaminar.Location = new System.Drawing.Point(290, 200);
            this.txbTipoCaminar.Name = "txbTipoCaminar";
            this.txbTipoCaminar.Size = new System.Drawing.Size(174, 20);
            this.txbTipoCaminar.TabIndex = 8;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(472, 125);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(72, 23);
            this.label16.TabIndex = 61;
            this.label16.Text = "Tamaño";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(286, 125);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(66, 23);
            this.label17.TabIndex = 60;
            this.label17.Text = "Genero";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(472, 76);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(158, 23);
            this.label18.TabIndex = 59;
            this.label18.Text = "Descripción Habitat";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(286, 76);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(107, 23);
            this.label19.TabIndex = 58;
            this.label19.Text = "Clasificación";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(472, 27);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(145, 23);
            this.label20.TabIndex = 57;
            this.label20.Text = "Nombre Cientifico";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(286, 27);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(129, 23);
            this.label21.TabIndex = 56;
            this.label21.Text = "Nombre Común";
            // 
            // txbNomCienMa
            // 
            this.txbNomCienMa.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbNomCienMa.Location = new System.Drawing.Point(476, 53);
            this.txbNomCienMa.Name = "txbNomCienMa";
            this.txbNomCienMa.Size = new System.Drawing.Size(141, 20);
            this.txbNomCienMa.TabIndex = 3;
            // 
            // txbNomComMa
            // 
            this.txbNomComMa.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbNomComMa.Location = new System.Drawing.Point(290, 53);
            this.txbNomComMa.Name = "txbNomComMa";
            this.txbNomComMa.Size = new System.Drawing.Size(150, 20);
            this.txbNomComMa.TabIndex = 2;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.label22.Location = new System.Drawing.Point(38, 3);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(199, 24);
            this.label22.TabIndex = 50;
            this.label22.Text = "NUEVO MAMIFERO";
            // 
            // pbMamifero
            // 
            this.pbMamifero.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pbMamifero.Location = new System.Drawing.Point(22, 85);
            this.pbMamifero.Name = "pbMamifero";
            this.pbMamifero.Size = new System.Drawing.Size(229, 150);
            this.pbMamifero.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbMamifero.TabIndex = 49;
            this.pbMamifero.TabStop = false;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.cbAcuristica);
            this.tabPage1.Controls.Add(this.cbTipoPez);
            this.tabPage1.Controls.Add(this.cbtipoesqueletopec);
            this.tabPage1.Controls.Add(this.cbHabitatPes1);
            this.tabPage1.Controls.Add(this.cbClasPes1);
            this.tabPage1.Controls.Add(this.label36);
            this.tabPage1.Controls.Add(this.btnAgregarPe);
            this.tabPage1.Controls.Add(this.label34);
            this.tabPage1.Controls.Add(this.label32);
            this.tabPage1.Controls.Add(this.label33);
            this.tabPage1.Controls.Add(this.label30);
            this.tabPage1.Controls.Add(this.txtNombreCienPe);
            this.tabPage1.Controls.Add(this.label29);
            this.tabPage1.Controls.Add(this.label28);
            this.tabPage1.Controls.Add(this.txtNombreComPe);
            this.tabPage1.Controls.Add(this.label27);
            this.tabPage1.Controls.Add(this.dgvPeces);
            this.tabPage1.Controls.Add(this.btnExplorarPe);
            this.tabPage1.Controls.Add(this.pbPeces);
            this.tabPage1.Controls.Add(this.label25);
            this.tabPage1.Controls.Add(this.txtRutaPe);
            this.tabPage1.Controls.Add(this.label24);
            this.tabPage1.Controls.Add(this.label23);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(640, 556);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Peces";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // cbAcuristica
            // 
            this.cbAcuristica.FormattingEnabled = true;
            this.cbAcuristica.Items.AddRange(new object[] {
            "Seleccionar",
            "Agua dulce",
            "Agua salobre"});
            this.cbAcuristica.Location = new System.Drawing.Point(290, 199);
            this.cbAcuristica.Name = "cbAcuristica";
            this.cbAcuristica.Size = new System.Drawing.Size(150, 21);
            this.cbAcuristica.TabIndex = 78;
            // 
            // cbTipoPez
            // 
            this.cbTipoPez.FormattingEnabled = true;
            this.cbTipoPez.Items.AddRange(new object[] {
            "Seleccionar",
            "Diadromicos",
            "Bentonicos",
            "pelagicos",
            "continentales"});
            this.cbTipoPez.Location = new System.Drawing.Point(475, 150);
            this.cbTipoPez.Name = "cbTipoPez";
            this.cbTipoPez.Size = new System.Drawing.Size(141, 21);
            this.cbTipoPez.TabIndex = 77;
            // 
            // cbtipoesqueletopec
            // 
            this.cbtipoesqueletopec.FormattingEnabled = true;
            this.cbtipoesqueletopec.Items.AddRange(new object[] {
            "Seleccionar",
            "Exoesqueleto",
            "Endoesqueleto",
            "Esqueleto Somatico",
            "Esqueleto Visceral"});
            this.cbtipoesqueletopec.Location = new System.Drawing.Point(290, 151);
            this.cbtipoesqueletopec.Name = "cbtipoesqueletopec";
            this.cbtipoesqueletopec.Size = new System.Drawing.Size(150, 21);
            this.cbtipoesqueletopec.TabIndex = 76;
            // 
            // cbHabitatPes1
            // 
            this.cbHabitatPes1.FormattingEnabled = true;
            this.cbHabitatPes1.Items.AddRange(new object[] {
            "Seleccionar",
            "Lago",
            "Rio",
            "Mar",
            "Oceano"});
            this.cbHabitatPes1.Location = new System.Drawing.Point(475, 100);
            this.cbHabitatPes1.Name = "cbHabitatPes1";
            this.cbHabitatPes1.Size = new System.Drawing.Size(141, 21);
            this.cbHabitatPes1.TabIndex = 75;
            // 
            // cbClasPes1
            // 
            this.cbClasPes1.FormattingEnabled = true;
            this.cbClasPes1.Items.AddRange(new object[] {
            "Seleccionar",
            "Ciclostomata",
            "Condictrios",
            "Osteictios",
            "Diadromicos ",
            "Bentonicos",
            "Pelagicos",
            "Continentales"});
            this.cbClasPes1.Location = new System.Drawing.Point(290, 100);
            this.cbClasPes1.Name = "cbClasPes1";
            this.cbClasPes1.Size = new System.Drawing.Size(150, 21);
            this.cbClasPes1.TabIndex = 74;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.label36.Location = new System.Drawing.Point(228, 254);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(135, 22);
            this.label36.TabIndex = 67;
            this.label36.Text = "LISTA PECES";
            // 
            // btnAgregarPe
            // 
            this.btnAgregarPe.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnAgregarPe.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnAgregarPe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgregarPe.ForeColor = System.Drawing.Color.White;
            this.btnAgregarPe.Location = new System.Drawing.Point(475, 199);
            this.btnAgregarPe.Name = "btnAgregarPe";
            this.btnAgregarPe.Size = new System.Drawing.Size(141, 24);
            this.btnAgregarPe.TabIndex = 73;
            this.btnAgregarPe.Text = "Agregar";
            this.btnAgregarPe.UseVisualStyleBackColor = false;
            this.btnAgregarPe.Click += new System.EventHandler(this.btnAgregarPe_Click);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(286, 173);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(145, 23);
            this.label34.TabIndex = 70;
            this.label34.Text = "Tipo de acurística";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(471, 124);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(75, 23);
            this.label32.TabIndex = 70;
            this.label32.Text = "Tipo Pez";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(286, 124);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(147, 23);
            this.label33.TabIndex = 71;
            this.label33.Text = "Tipo de Esqueleto";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(471, 75);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(158, 23);
            this.label30.TabIndex = 68;
            this.label30.Text = "Descripción Habitat";
            // 
            // txtNombreCienPe
            // 
            this.txtNombreCienPe.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombreCienPe.Location = new System.Drawing.Point(475, 52);
            this.txtNombreCienPe.Name = "txtNombreCienPe";
            this.txtNombreCienPe.Size = new System.Drawing.Size(141, 20);
            this.txtNombreCienPe.TabIndex = 68;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(471, 26);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(145, 23);
            this.label29.TabIndex = 68;
            this.label29.Text = "Nombre Cientifico";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(286, 75);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(107, 23);
            this.label28.TabIndex = 68;
            this.label28.Text = "Clasificación";
            // 
            // txtNombreComPe
            // 
            this.txtNombreComPe.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombreComPe.Location = new System.Drawing.Point(290, 52);
            this.txtNombreComPe.Name = "txtNombreComPe";
            this.txtNombreComPe.Size = new System.Drawing.Size(150, 20);
            this.txtNombreComPe.TabIndex = 68;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(286, 26);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(129, 23);
            this.label27.TabIndex = 68;
            this.label27.Text = "Nombre Común";
            // 
            // dgvPeces
            // 
            this.dgvPeces.AllowUserToAddRows = false;
            this.dgvPeces.AllowUserToDeleteRows = false;
            this.dgvPeces.Location = new System.Drawing.Point(11, 279);
            this.dgvPeces.Name = "dgvPeces";
            this.dgvPeces.ReadOnly = true;
            this.dgvPeces.RowHeadersVisible = false;
            this.dgvPeces.RowTemplate.Height = 100;
            this.dgvPeces.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPeces.Size = new System.Drawing.Size(623, 267);
            this.dgvPeces.TabIndex = 68;
            // 
            // btnExplorarPe
            // 
            this.btnExplorarPe.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnExplorarPe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExplorarPe.ForeColor = System.Drawing.Color.White;
            this.btnExplorarPe.Location = new System.Drawing.Point(177, 29);
            this.btnExplorarPe.Name = "btnExplorarPe";
            this.btnExplorarPe.Size = new System.Drawing.Size(75, 23);
            this.btnExplorarPe.TabIndex = 27;
            this.btnExplorarPe.Text = "Explorar";
            this.btnExplorarPe.UseVisualStyleBackColor = false;
            this.btnExplorarPe.Click += new System.EventHandler(this.btnExplorarPe_Click);
            // 
            // pbPeces
            // 
            this.pbPeces.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pbPeces.Location = new System.Drawing.Point(23, 85);
            this.pbPeces.Name = "pbPeces";
            this.pbPeces.Size = new System.Drawing.Size(229, 150);
            this.pbPeces.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbPeces.TabIndex = 50;
            this.pbPeces.TabStop = false;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 241);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(622, 13);
            this.label25.TabIndex = 66;
            this.label25.Text = resources.GetString("label25.Text");
            // 
            // txtRutaPe
            // 
            this.txtRutaPe.Location = new System.Drawing.Point(23, 59);
            this.txtRutaPe.Name = "txtRutaPe";
            this.txtRutaPe.Size = new System.Drawing.Size(229, 20);
            this.txtRutaPe.TabIndex = 49;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(19, 30);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(126, 19);
            this.label24.TabIndex = 47;
            this.label24.Text = "Foto del Animal";
            this.label24.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.label23.Location = new System.Drawing.Point(58, 3);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(157, 24);
            this.label23.TabIndex = 28;
            this.label23.Text = "NUEVO PECES";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.cbReproduccionAntrop);
            this.tabPage3.Controls.Add(this.cbAlimentacionAntrop);
            this.tabPage3.Controls.Add(this.cbPuedeSer);
            this.tabPage3.Controls.Add(this.cbHabitatAntropodo);
            this.tabPage3.Controls.Add(this.cbClasAntropodo);
            this.tabPage3.Controls.Add(this.label44);
            this.tabPage3.Controls.Add(this.btnAgregarAntropodo);
            this.tabPage3.Controls.Add(this.label43);
            this.tabPage3.Controls.Add(this.label41);
            this.tabPage3.Controls.Add(this.label42);
            this.tabPage3.Controls.Add(this.label40);
            this.tabPage3.Controls.Add(this.txtNombreCientificoAntro);
            this.tabPage3.Controls.Add(this.label39);
            this.tabPage3.Controls.Add(this.label38);
            this.tabPage3.Controls.Add(this.txtNomComunAntro);
            this.tabPage3.Controls.Add(this.label37);
            this.tabPage3.Controls.Add(this.dgvAntropodos);
            this.tabPage3.Controls.Add(this.label35);
            this.tabPage3.Controls.Add(this.btnExplorarAntro);
            this.tabPage3.Controls.Add(this.pbAntropodos);
            this.tabPage3.Controls.Add(this.txtRutaAntropodos);
            this.tabPage3.Controls.Add(this.label26);
            this.tabPage3.Controls.Add(this.label31);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(640, 556);
            this.tabPage3.TabIndex = 3;
            this.tabPage3.Text = "Antropodos";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // cbReproduccionAntrop
            // 
            this.cbReproduccionAntrop.FormattingEnabled = true;
            this.cbReproduccionAntrop.Items.AddRange(new object[] {
            "Seleccionar",
            "Insectos",
            "Crustáceos",
            "Arácnidos",
            "Miriápodos"});
            this.cbReproduccionAntrop.Location = new System.Drawing.Point(290, 197);
            this.cbReproduccionAntrop.Name = "cbReproduccionAntrop";
            this.cbReproduccionAntrop.Size = new System.Drawing.Size(143, 21);
            this.cbReproduccionAntrop.TabIndex = 79;
            // 
            // cbAlimentacionAntrop
            // 
            this.cbAlimentacionAntrop.FormattingEnabled = true;
            this.cbAlimentacionAntrop.Items.AddRange(new object[] {
            "Seleccionar",
            "Orugas",
            "Algas",
            "Tallos",
            "Fruta"});
            this.cbAlimentacionAntrop.Location = new System.Drawing.Point(475, 148);
            this.cbAlimentacionAntrop.Name = "cbAlimentacionAntrop";
            this.cbAlimentacionAntrop.Size = new System.Drawing.Size(150, 21);
            this.cbAlimentacionAntrop.TabIndex = 78;
            // 
            // cbPuedeSer
            // 
            this.cbPuedeSer.FormattingEnabled = true;
            this.cbPuedeSer.Items.AddRange(new object[] {
            "Seleccionar",
            "Terrestre",
            "Acuático"});
            this.cbPuedeSer.Location = new System.Drawing.Point(290, 149);
            this.cbPuedeSer.Name = "cbPuedeSer";
            this.cbPuedeSer.Size = new System.Drawing.Size(143, 21);
            this.cbPuedeSer.TabIndex = 77;
            // 
            // cbHabitatAntropodo
            // 
            this.cbHabitatAntropodo.FormattingEnabled = true;
            this.cbHabitatAntropodo.Items.AddRange(new object[] {
            "Seleccionar",
            "Mar",
            "Montañoso",
            "Desierto",
            "Selva Tropical"});
            this.cbHabitatAntropodo.Location = new System.Drawing.Point(475, 98);
            this.cbHabitatAntropodo.Name = "cbHabitatAntropodo";
            this.cbHabitatAntropodo.Size = new System.Drawing.Size(150, 21);
            this.cbHabitatAntropodo.TabIndex = 76;
            // 
            // cbClasAntropodo
            // 
            this.cbClasAntropodo.FormattingEnabled = true;
            this.cbClasAntropodo.Items.AddRange(new object[] {
            "Seleccionar",
            "Insecto",
            "Crustaceo",
            "Aracnido",
            "Miriapodos"});
            this.cbClasAntropodo.Location = new System.Drawing.Point(290, 97);
            this.cbClasAntropodo.Name = "cbClasAntropodo";
            this.cbClasAntropodo.Size = new System.Drawing.Size(143, 21);
            this.cbClasAntropodo.TabIndex = 75;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.label44.Location = new System.Drawing.Point(228, 254);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(205, 22);
            this.label44.TabIndex = 67;
            this.label44.Text = "LISTA ANTROPODOS";
            // 
            // btnAgregarAntropodo
            // 
            this.btnAgregarAntropodo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnAgregarAntropodo.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnAgregarAntropodo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgregarAntropodo.ForeColor = System.Drawing.Color.White;
            this.btnAgregarAntropodo.Location = new System.Drawing.Point(475, 197);
            this.btnAgregarAntropodo.Name = "btnAgregarAntropodo";
            this.btnAgregarAntropodo.Size = new System.Drawing.Size(141, 27);
            this.btnAgregarAntropodo.TabIndex = 74;
            this.btnAgregarAntropodo.Text = "Agregar";
            this.btnAgregarAntropodo.UseVisualStyleBackColor = false;
            this.btnAgregarAntropodo.Click += new System.EventHandler(this.btnAgregarAntropodo_Click);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Black;
            this.label43.Location = new System.Drawing.Point(286, 171);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(185, 23);
            this.label43.TabIndex = 74;
            this.label43.Text = "clasificación artrópodo";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(471, 122);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(109, 23);
            this.label41.TabIndex = 72;
            this.label41.Text = "Alimentacion";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(286, 122);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(91, 23);
            this.label42.TabIndex = 73;
            this.label42.Text = "Puede ser:";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(471, 73);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(158, 23);
            this.label40.TabIndex = 69;
            this.label40.Text = "Descripción Habitat";
            // 
            // txtNombreCientificoAntro
            // 
            this.txtNombreCientificoAntro.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombreCientificoAntro.Location = new System.Drawing.Point(475, 50);
            this.txtNombreCientificoAntro.Name = "txtNombreCientificoAntro";
            this.txtNombreCientificoAntro.Size = new System.Drawing.Size(150, 20);
            this.txtNombreCientificoAntro.TabIndex = 69;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(471, 24);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(145, 23);
            this.label39.TabIndex = 68;
            this.label39.Text = "Nombre Cientifico";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(286, 73);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(107, 23);
            this.label38.TabIndex = 68;
            this.label38.Text = "Clasificación";
            // 
            // txtNomComunAntro
            // 
            this.txtNomComunAntro.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNomComunAntro.Location = new System.Drawing.Point(290, 50);
            this.txtNomComunAntro.Name = "txtNomComunAntro";
            this.txtNomComunAntro.Size = new System.Drawing.Size(150, 20);
            this.txtNomComunAntro.TabIndex = 68;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(286, 24);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(129, 23);
            this.label37.TabIndex = 68;
            this.label37.Text = "Nombre Común";
            // 
            // dgvAntropodos
            // 
            this.dgvAntropodos.AllowUserToAddRows = false;
            this.dgvAntropodos.AllowUserToDeleteRows = false;
            this.dgvAntropodos.Location = new System.Drawing.Point(11, 279);
            this.dgvAntropodos.Name = "dgvAntropodos";
            this.dgvAntropodos.ReadOnly = true;
            this.dgvAntropodos.RowHeadersVisible = false;
            this.dgvAntropodos.RowTemplate.Height = 100;
            this.dgvAntropodos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAntropodos.Size = new System.Drawing.Size(623, 267);
            this.dgvAntropodos.TabIndex = 68;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(12, 236);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(622, 13);
            this.label35.TabIndex = 66;
            this.label35.Text = resources.GetString("label35.Text");
            // 
            // btnExplorarAntro
            // 
            this.btnExplorarAntro.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnExplorarAntro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExplorarAntro.ForeColor = System.Drawing.Color.White;
            this.btnExplorarAntro.Location = new System.Drawing.Point(178, 28);
            this.btnExplorarAntro.Name = "btnExplorarAntro";
            this.btnExplorarAntro.Size = new System.Drawing.Size(75, 23);
            this.btnExplorarAntro.TabIndex = 27;
            this.btnExplorarAntro.Text = "Explorar";
            this.btnExplorarAntro.UseVisualStyleBackColor = false;
            this.btnExplorarAntro.Click += new System.EventHandler(this.btnExplorarAntro_Click);
            // 
            // pbAntropodos
            // 
            this.pbAntropodos.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pbAntropodos.Location = new System.Drawing.Point(24, 83);
            this.pbAntropodos.Name = "pbAntropodos";
            this.pbAntropodos.Size = new System.Drawing.Size(229, 150);
            this.pbAntropodos.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbAntropodos.TabIndex = 51;
            this.pbAntropodos.TabStop = false;
            // 
            // txtRutaAntropodos
            // 
            this.txtRutaAntropodos.Location = new System.Drawing.Point(24, 57);
            this.txtRutaAntropodos.Name = "txtRutaAntropodos";
            this.txtRutaAntropodos.ReadOnly = true;
            this.txtRutaAntropodos.Size = new System.Drawing.Size(229, 20);
            this.txtRutaAntropodos.TabIndex = 27;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(20, 28);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(126, 19);
            this.label26.TabIndex = 48;
            this.label26.Text = "Foto del Animal";
            this.label26.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.label31.Location = new System.Drawing.Point(20, 3);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(233, 24);
            this.label31.TabIndex = 68;
            this.label31.Text = "NUEVO ANTROPODOS";
            // 
            // fileExplorer
            // 
            this.fileExplorer.FileName = "openFileDialog1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(669, 598);
            this.Controls.Add(this.tabControl1);
            this.ForeColor = System.Drawing.Color.Black;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Zoológico";
            this.tabControl1.ResumeLayout(false);
            this.TabReptiles.ResumeLayout(false);
            this.TabReptiles.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReptiles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbReptil)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMamiferos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMamifero)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPeces)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPeces)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAntropodos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAntropodos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage TabReptiles;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox txbRutaRe;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnExplorarRe;
        private System.Windows.Forms.DataGridView dgvReptiles;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnAgregarRe;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txbColor;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txbDesHabitadRe;
        private System.Windows.Forms.TextBox txbNomCienRe;
        private System.Windows.Forms.TextBox txbNomComRe;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pbReptil;
        private System.Windows.Forms.TextBox txbTamaño;
        private System.Windows.Forms.TextBox txbRutaMa;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnExplorarMa;
        private System.Windows.Forms.DataGridView dgvMamiferos;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btnAgregarMa;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txbTipoCaminar;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txbNomCienMa;
        private System.Windows.Forms.TextBox txbNomComMa;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.PictureBox pbMamifero;
        private System.Windows.Forms.OpenFileDialog fileExplorer;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btnExplorarPe;
        private System.Windows.Forms.PictureBox pbPeces;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtRutaPe;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button btnAgregarPe;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtNombreCienPe;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtNombreComPe;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.DataGridView dgvPeces;
        private System.Windows.Forms.Button btnAgregarAntropodo;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox txtNombreCientificoAntro;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox txtNomComunAntro;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.DataGridView dgvAntropodos;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Button btnExplorarAntro;
        private System.Windows.Forms.PictureBox pbAntropodos;
        private System.Windows.Forms.TextBox txtRutaAntropodos;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.ComboBox cbTipoEscama;
        private System.Windows.Forms.ComboBox cbClasReptil1;
        private System.Windows.Forms.ComboBox cbGeneroMamifero;
        private System.Windows.Forms.ComboBox cbClasMamifero;
        private System.Windows.Forms.ComboBox cbClasPes1;
        private System.Windows.Forms.ComboBox cbClasAntropodo;
        private System.Windows.Forms.ComboBox cbClasReptil2;
        private System.Windows.Forms.ComboBox cbHabitatMamifero;
        private System.Windows.Forms.ComboBox cbHabitatPes1;
        private System.Windows.Forms.ComboBox cbHabitatAntropodo;
        private System.Windows.Forms.ComboBox cbAcuristica;
        private System.Windows.Forms.ComboBox cbTipoPez;
        private System.Windows.Forms.ComboBox cbtipoesqueletopec;
        private System.Windows.Forms.ComboBox cbReproduccionAntrop;
        private System.Windows.Forms.ComboBox cbAlimentacionAntrop;
        private System.Windows.Forms.ComboBox cbPuedeSer;
    }
}


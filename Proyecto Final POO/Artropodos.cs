using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Proyecto_Final_POO
{
    /**
     * 
     * -------------------- POO_01T - G06
     * -------------------- INTEGRANTES
     * ** Román Antonio González Montano    | GM181937
     * ** Kevin Osvaldo Marroquín Medrano   | MM161915
     * ** Juan José Magaña Moreira          | MM210126
     * ** Ricardo Antonio Quijano Vasquez   | QV211321
     * ** Kevin Osmaro Sibrián López        | Sl210844
     * 
     * -------------------- URL SOLUCIÓN
     * ** https://gitlab.com/marroquin_oz/zoologico_app-poo01t_g06
     * 
     */
    class Artropodos : Animales
    {
        private string puedeSer;
        private string alimentacion;
        private string clasificacionArtropodo;
        private XmlDocument database = new XmlDocument();

        // GETTERS Y SETTERS SIN USAR, YA QUE TENEMOS METODOS ESPECIFICOS PARA OBTENER Y ASIGNAR LAS PROPIEDADES

        public override int validarVacios(string nombreComun, string nombreCientifico, string clasificacion, string habitad, string puedeSer, string alimentacion, string clasificacionArtropodo, string rutaFoto)
        {
            if (nombreComun == "" || nombreCientifico == "" || clasificacion == "Seleccionar" || habitad == "Seleccionar" || puedeSer == "Seleccionar" || alimentacion == "Seleccionar" || clasificacionArtropodo == "Seleccionar")
            {
                MessageBox.Show("favor completar todos los campos");
                return 0;
            }
            else
            {
                //Validamos que se haya seleccionado una imagen
                if (rutaFoto == "")
                {
                    MessageBox.Show("Seleccione una imagen para el animal a registrar");
                    return 0;
                }

                return 1;
            }
        }

        public override bool setAnimal(
            string nombreComun,
            string nombreCientifico,
            string clasificacion,
            string habitad,
            string param1,
            string param2,
            string param3,
            string rutaFoto
        )
        {
            this.NombreComun = nombreComun;
            this.NombreCientifico = nombreCientifico;
            this.Clasificacion = clasificacion;
            this.DescripcionHabitad = habitad;
            this.puedeSer = param1;
            this.alimentacion = param2;
            this.clasificacionArtropodo = param3;
            this.Foto = rutaFoto;

            return true;
        }

        public override Dictionary<string, Object> getAnimal()
        {
            return new Dictionary<string, Object>() {
                {"nombreComun", this.NombreComun},
                {"nombreCientifico", this.NombreCientifico},
                {"clasificacion", this.Clasificacion},
                {"habitad", this.DescripcionHabitad},
                {"puedeSer", this.puedeSer},
                {"alimentacion", this.alimentacion},
                {"clasificacionArtropodo", this.clasificacionArtropodo},
                {"rutaFoto", this.Foto}
            };
        }

        public override void Save()
        {
            database.Load("database.xml");
            XmlNode parent = database.CreateNode(XmlNodeType.Element, "Animal", "");
            /**************************/
            parent.AppendChild(database.CreateElement("nombreComun"));
            parent.LastChild.InnerText = this.NombreComun;
            parent.AppendChild(database.CreateElement("nombreCientifico"));
            parent.LastChild.InnerText = this.NombreCientifico;
            parent.AppendChild(database.CreateElement("clasificacion"));
            parent.LastChild.InnerText = this.Clasificacion;
            parent.AppendChild(database.CreateElement("habitad"));
            parent.LastChild.InnerText = this.DescripcionHabitad;
            parent.AppendChild(database.CreateElement("puedeSer"));
            parent.LastChild.InnerText = this.puedeSer;
            parent.AppendChild(database.CreateElement("alimentacion"));
            parent.LastChild.InnerText = this.alimentacion;
            parent.AppendChild(database.CreateElement("clasificacionArtropodo"));
            parent.LastChild.InnerText = this.clasificacionArtropodo;
            parent.AppendChild(database.CreateElement("rutaFoto"));
            parent.LastChild.InnerText = this.Foto;
            /*************************/
            database.SelectSingleNode(@"Database/Antropodos[1]").AppendChild(parent);
            database.Save("database.xml");
        }

    }
}

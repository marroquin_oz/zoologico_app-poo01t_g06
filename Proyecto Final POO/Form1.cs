﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Proyecto_Final_POO
{
    /**
     * 
     * -------------------- POO_01T - G06
     * -------------------- INTEGRANTES
     * ** Román Antonio González Montano    | GM181937
     * ** Kevin Osvaldo Marroquín Medrano   | MM161915
     * ** Juan José Magaña Moreira          | MM210126
     * ** Ricardo Antonio Quijano Vasquez   | QV211321
     * ** Kevin Osmaro Sibrián López        | Sl210844
     * 
     * -------------------- URL SOLUCIÓN
     * ** https://gitlab.com/marroquin_oz/zoologico_app-poo01t_g06
     * 
     */

    public partial class Form1 : Form
    {
        //XML donde se guardara cada animal agregado, y alimentaran la data de los DataGridView
        XmlDocument database = new XmlDocument();

        public Form1()
        {
            InitializeComponent();

            database.Load("database.xml");
            //Mamiferos

            dgvMamiferos.Columns.Add( new DataGridViewImageColumn() { Name = "Foto", HeaderText = "Foto" } ); //Creamos una columna para poder mostrar imagenes
            dgvMamiferos.Columns.Add("NombreComun", "Nombre Común");
            dgvMamiferos.Columns.Add("NombreCientifico", "Nombre Cientifico");
            dgvMamiferos.Columns.Add("Clasificacion", "Clasificacion");
            dgvMamiferos.Columns.Add("DesHabitat", "Descripcion Habitat");
            dgvMamiferos.Columns.Add("Genero", "Genero");
            dgvMamiferos.Columns.Add("Tamanio", "Tamaño");
            dgvMamiferos.Columns.Add("ApoyoCaminar", "Tipo Apoyo al Caminar");
            //Reptiles
            dgvReptiles.Columns.Add( new DataGridViewImageColumn() { Name = "Foto", HeaderText = "Foto" } ); //Creamos una columna para poder mostrar imagenes
            dgvReptiles.Columns.Add("NombreComun", "Nombre Común");
            dgvReptiles.Columns.Add("NombreCientifico", "Nombre Cientifico");
            dgvReptiles.Columns.Add("Clasificacion", "Clasificacion");
            dgvReptiles.Columns.Add("DesHabitat", "Descripcion Habitat");
            dgvReptiles.Columns.Add("ClasReptil", "Clasificacion Reptil");
            dgvReptiles.Columns.Add("TipoEscama", "Tipo Escama");
            dgvReptiles.Columns.Add("Color", "Color");
            //Peces
            dgvPeces.Columns.Add( new DataGridViewImageColumn() { Name = "Foto", HeaderText = "Foto" } ); //Creamos una columna para poder mostrar imagenes
            dgvPeces.Columns.Add("NombreComun", "Nombre Común");
            dgvPeces.Columns.Add("NombreCientifico", "Nombre Cientifico");
            dgvPeces.Columns.Add("Clasificacion", "Clasificacion");
            dgvPeces.Columns.Add("DesHabitat", "Descripcion Habitat");
            dgvPeces.Columns.Add("Tipo de Esqueleto", "Tipo de Esqueleto");
            dgvPeces.Columns.Add("Alimentacion", "Alimentacion");
            dgvPeces.Columns.Add("Reproduccion", "Reproduccion");
            //Antropodos
            dgvAntropodos.Columns.Add( new DataGridViewImageColumn() { Name = "Foto", HeaderText = "Foto" } ); //Creamos una columna para poder mostrar imagenes
            dgvAntropodos.Columns.Add("NombreComun", "Nombre Común");
            dgvAntropodos.Columns.Add("NombreCientifico", "Nombre Cientifico");
            dgvAntropodos.Columns.Add("Clasificacion", "Clasificacion");
            dgvAntropodos.Columns.Add("DesHabitat", "Descripcion Habitat");
            dgvAntropodos.Columns.Add("Tipo de esqueleto", "Tipo de esqueleto");
            dgvAntropodos.Columns.Add("Alimentacion", "Alimentacion");
            dgvAntropodos.Columns.Add("Reproduccion", "Reproduccion");

            //Combobox Mamíferos
            cbClasMamifero.SelectedIndex = 0;
            cbHabitatMamifero.SelectedIndex = 0;
            cbGeneroMamifero.SelectedIndex = 0;
            //Combobox Reptiles
            cbClasReptil2.SelectedIndex = 0;
            cbClasReptil1.SelectedIndex = 0;
            cbTipoEscama.SelectedIndex = 0;
            //Combobox Peces
            cbClasPes1.SelectedIndex = 0;
            cbHabitatPes1.SelectedIndex = 0;
            cbtipoesqueletopec.SelectedIndex = 0;
            cbTipoPez.SelectedIndex = 0;
            cbAcuristica.SelectedIndex = 0;
            //Combobox Artrópodos
            cbClasAntropodo.SelectedIndex = 0;
            cbHabitatAntropodo.SelectedIndex = 0;
            cbPuedeSer.SelectedIndex = 0;
            cbAlimentacionAntrop.SelectedIndex = 0;
            cbReproduccionAntrop.SelectedIndex = 0;

            readXML("Reptiles");
            readXML("Mamiferos");
            readXML("Peces");
            readXML("Antropodos");
        }

        private void readXML(string key) {
            XmlNodeList i = database.SelectNodes("Database/"+ key +"/Animal");
            foreach (XmlNode no in i) {
                Dictionary<string, Object> auxlist = new Dictionary<string, Object>();
                XmlNodeList j = no.SelectNodes("*");
                foreach (XmlNode cno in j)
                {
                    auxlist.Add(cno.Name, cno.InnerText);
                }
                UpdateLists(key.ToLower(), auxlist);
            }
        }
        //Explora la ruta de la imagen
        private void Explorar(PictureBox Imagen, TextBox txbRuta)
        {
            fileExplorer.Title = "Ingrese imagen del animal";
            fileExplorer.FileName = "";
            fileExplorer.Filter = "Image Files(*.JPG;*.PNG)|*.JPG;*.PNG";
            if (fileExplorer.ShowDialog()==DialogResult.OK)
            {
                Imagen.ImageLocation = fileExplorer.FileName;
                txbRuta.Text = fileExplorer.FileName;
            }
        }

        //Lenamos la listas y actualizaremos DatasGridViews
        private void UpdateLists(string type, Dictionary<string, Object> lista) {
            switch (type)
            {
                case "reptiles":
                    dgvReptiles.Rows.Add( //Actualizar DataGridView
                        new Bitmap(Image.FromFile((string)lista["rutaFoto"]), new Size(100, 100)),
                        lista["nombreComun"],
                        lista["nombreCientifico"],
                        lista["clasificacion"],
                        lista["habitad"],
                        lista["tipoReptil"],
                        lista["tipoEscama"],
                        lista["color"]
                    );
                    break;
                case "mamiferos":
                    dgvMamiferos.Rows.Add( //Actualizar DataGridView
                        Bitmap.FromFile((string)lista["rutaFoto"]),
                        lista["nombreComun"],
                        lista["nombreCientifico"],
                        lista["clasificacion"],
                        lista["habitad"],
                        lista["genero"],
                        lista["tamano"],
                        lista["tipoApoyo"]
                    );
                    break;
                case "peces":
                    dgvPeces.Rows.Add( //Actualizar DataGridView
                        Bitmap.FromFile((string)lista["rutaFoto"]),
                        lista["nombreComun"],
                        lista["nombreCientifico"],
                        lista["clasificacion"],
                        lista["habitad"],
                        lista["tipoEsqueleto"],
                        lista["tipoPez"],
                        lista["tipoAcuristica"]
                    );
                    break;
                case "antropodos":
                    dgvAntropodos.Rows.Add( //Actualizar DataGridView
                        Bitmap.FromFile((string)lista["rutaFoto"]),
                        lista["nombreComun"],
                        lista["nombreCientifico"],
                        lista["clasificacion"],
                        lista["habitad"],
                        lista["puedeSer"],
                        lista["alimentacion"],
                        lista["clasificacionArtropodo"]
                    );
                    break;
                default:
                    MessageBox.Show("Ha ocurrido un error al llenar lista, ingrese un nombre de lista correcto.");
                    break;
            }
        }


        private void btnExplorarMa_Click(object sender, EventArgs e)
        {
            Explorar(pbMamifero, txbRutaMa);
        }

        private void btnExplorarRe_Click(object sender, EventArgs e)
        {
            Explorar(pbReptil, txbRutaRe);
        }

        private void btnExplorarPe_Click(object sender, EventArgs e)
        {
            Explorar(pbPeces, txtRutaPe);
        }

        private void btnExplorarAntro_Click(object sender, EventArgs e)
        {
            Explorar(pbAntropodos, txtRutaAntropodos);
        }

        // METODOS PARA AGREGAR
        private void btnAgregarRe_Click(object sender, EventArgs e)
        {
            Reptiles reptil = new Reptiles();

            //Mandamos a llamar función que se encargara de validar los campos
            int validar = reptil.validarVacios(txbNomComRe.Text, txbNomCienRe.Text, cbClasReptil2.Text, txbDesHabitadRe.Text, cbClasReptil1.Text, cbTipoEscama.Text, txbColor.Text, txbRutaRe.Text);
            bool resp = false;
            if (validar == 1)
            {
                //Registrar el animal
                resp = reptil.setAnimal(txbNomComRe.Text, txbNomCienRe.Text, cbClasReptil2.Text, txbDesHabitadRe.Text, cbClasReptil1.Text, cbTipoEscama.Text, txbColor.Text, txbRutaRe.Text);
            }
            if (resp)
            {
                //LLENAR LISTA
                UpdateLists("reptiles", reptil.getAnimal());
                reptil.Save(); //Guardar en XML
            }
        }

        private void btnAgregarMa_Click(object sender, EventArgs e)
        {
            Mamiferos mamifero = new Mamiferos();

            //Mandamos a llamar función que se encargara de validar los campos mamiferos
            int validar = mamifero.validarVacios(txbNomComMa.Text, txbNomCienMa.Text, cbClasMamifero.Text, cbHabitatMamifero.Text, cbGeneroMamifero.Text, txbTamaño.Text, txbTipoCaminar.Text, txbRutaMa.Text);
            bool resp = false;
            if (validar == 1)
            {
                //Registrar el animal
                resp = mamifero.setAnimal(txbNomComMa.Text, txbNomCienMa.Text, cbClasMamifero.Text, cbHabitatMamifero.Text, cbGeneroMamifero.Text, txbTamaño.Text, txbTipoCaminar.Text, txbRutaMa.Text);
            }
            if (resp)
            {
                //LLENAR LISTA
                UpdateLists("mamiferos", mamifero.getAnimal());
                mamifero.Save(); //Guardar en XML
            }
        }

        private void btnAgregarPe_Click(object sender, EventArgs e)
        {
            Peces pez = new Peces();

            //Mandamos a llamar función que se encargara de validar los campos
            int validar = pez.validarVacios(txtNombreComPe.Text, txtNombreCienPe.Text, cbClasPes1.Text, cbHabitatPes1.Text, cbtipoesqueletopec.Text, cbTipoPez.Text, cbAcuristica.Text, txtRutaPe.Text);
            bool resp = false;
            if (validar == 1)
            {
                //Registrar el animal
                resp = pez.setAnimal(txtNombreComPe.Text, txtNombreCienPe.Text, cbClasPes1.Text, cbHabitatPes1.Text, cbtipoesqueletopec.Text, cbTipoPez.Text, cbAcuristica.Text, txtRutaPe.Text);
            }
            if (resp)
            {
                //LLENAR LISTA
                UpdateLists("peces", pez.getAnimal());
                pez.Save(); //Guardar en XML
            }
        }

        private void btnAgregarAntropodo_Click(object sender, EventArgs e)
        {
            Artropodos antropodos = new Artropodos();

            //Mandamos a llamar función que se encargara de validar los campos
            int validar = antropodos.validarVacios(txtNomComunAntro.Text, txtNombreCientificoAntro.Text, cbClasAntropodo.Text, cbHabitatAntropodo.Text, cbPuedeSer.Text, cbAlimentacionAntrop.Text, cbReproduccionAntrop.Text, txtRutaAntropodos.Text);
            bool resp = false;
            if (validar == 1)
            {
                //Registrar el animal
                resp = antropodos.setAnimal(txtNomComunAntro.Text, txtNombreCientificoAntro.Text, cbClasAntropodo.Text, cbHabitatAntropodo.Text, cbPuedeSer.Text, cbAlimentacionAntrop.Text, cbReproduccionAntrop.Text, txtRutaAntropodos.Text);
            }
            if (resp)
            {
                //LLENAR LISTA
                UpdateLists("antropodos", antropodos.getAnimal());
                antropodos.Save(); //Guardar en XML
            }
        }

    }
}
